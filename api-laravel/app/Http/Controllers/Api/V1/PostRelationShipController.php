<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Resources\CommentResource;
use App\Http\Resources\UserResource;
use App\Models\Post;
use App\Http\Controllers\Controller;

class PostRelationShipController extends Controller
{
    public function author(Post $post)
    {
        return new UserResource($post->author);
    }

    public function comments(Post $post)
    {
        return CommentResource::collection($post->comments);
    }
}
