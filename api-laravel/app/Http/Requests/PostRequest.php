<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title" => 'required',
            "content" => 'required',
            "author_id" => 'required'
        ];
    }

    public function failedValidation(Validator $validator)// es para cambiar el comportamiento donde nos redirecciona a la página anterior con los errores obtenidps
    {
        throw new HttpResponseException(response()->json($validator->errors(), 422)); //arroja un json con los errores y un status
    }
}
