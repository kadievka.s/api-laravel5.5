<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\Auth;

class PostResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = Auth::user();

        return [
            'type' => $this->getTable(),
            'id' => $this->id,
            'attributes' => [
                'title' => $this->title,
            ],

            $this->mergeWhen(($this->isAuthorLoaded() && $this->isCommentLoaded()), [
                'relationships' => new PostsRelationshipResource($this),
            ]),

            'links' => [
                'self' => route('posts.show', ['post' => $this->id]),
            ]

        ];
    }
}
