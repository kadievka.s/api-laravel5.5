<?php

use App\Models\Post;
use App\User;
use Faker\Generator as Faker;

$factory->define(App\Models\Comment::class, function (Faker $faker) {
    return [
        'content' => $faker->text(200),
        'user_id' => User::all()->random(),
        'post_id' => Post::all()->random()
    ];
});
