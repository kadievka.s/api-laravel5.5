<?php

use App\User;
use Faker\Generator as Faker;

$factory->define(App\Models\Post::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(6, true),
        'author_id' => User::all()->random(),
        'content' => $faker->text(200)
    ];
});
