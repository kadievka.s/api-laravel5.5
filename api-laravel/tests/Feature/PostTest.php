<?php

namespace Tests\Feature;

use App\Models\Post;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PostTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /**
     * @test void
     */
    public function stores_post()
    {
        $user = create('App\User');

        $data = [
            'title' => $this->faker->sentence(6, true),
            'content' => $this->faker->text(200),
            'author_id' => $user->id
        ];//atributos al POST

        $response = $this->json('POST', $this->baseUrl . "posts", $data);// parametros: VERBO de la petición, URL hacia dónde hace la petición, arreglo

        //Validaciones

        $response->assertStatus(200);
        $this->assertDatabaseHas('posts', $data);

        $post = Post::all()->first();

        //$response->assertJson([
        //    'data' => [
        //        'id' => $post->id,
        //        'title' => $post->title
        //    ]
        //]);

        $response->assertJson([
            'type' => 'posts',
            'id' => $post->id,
            'attributes' => [
                'title' => $post->title
            ],
            'links' => [
                'self' => "http://localhost/api/v1/posts/{$post->id}"
            ],
        ]);

    }

    /**
     * @test void
     */
    public function deletes_post()
    {
        create('App\User');//creo un usuario
        $post = create('App\Models\Post');//creo un post con ese usuario

        $response = $this->json('DELETE', $this->baseUrl . "posts/{$post->id}");

        $response->assertStatus(204);

        $this->assertNull(Post::find($post->id));

    }

    /**
     * @test void
     */
    public function updates_post()
    {
        $data = [
            'title' => $this->faker->sentence(6, true),
            'content' => $this->faker->text(200)
        ];

        create('App\User');//creo un usuario
        $post = create('App\Models\Post');//creo un post con ese usuario

        $response = $this->json('PUT', $this->baseUrl . "posts/{$post->id}", $data);
        $response->assertStatus(200);

        $post = $post->fresh();

        $this->assertEquals($post->title, $data['title']);
        $this->assertEquals($post->content, $data['content']);

    }

    /**
     * @test
     */
    public function shows_post()
    {
        create('App\User');
        $post = create('App\Models\Post');

        $response = $this->json('GET', $this->baseUrl . "posts/{$post->id}");
        $response->assertStatus(200);
        //$response->assertJson([
        //    'data' => [
        //        'id' => $post->id,
        //        'title' => $post->title
        //    ]
        //]);
        $response->assertJson([
            'type' => 'posts',
            'id' => $post->id,
            'attributes' => [
                'title' => $post->title
            ],
            'links' => [
                'self' => "http://localhost/api/v1/posts/{$post->id}"
            ],
        ]);

    }
}
